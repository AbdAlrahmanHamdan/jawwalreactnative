import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import LoginScreen from './LoginScreen';
import MainScreen from './MainScreen';
import SplashScreen from './SplashScreen';
import {DrawerContent} from './DrawerContent';

const Drawer = createDrawerNavigator();

export default function DrawerScreen() {
    return (
    <Drawer.Navigator drawerContent={props => <DrawerContent {...props}/>}>
        <Drawer.Screen name="الصفحة الرئيسية" component={MainScreen} />
        <Drawer.Screen name="تغير الرقم السري" component={SplashScreen} />
        <Drawer.Screen name="الاعدادات" component={SplashScreen} />
        <Drawer.Screen name="تسجيل الخروج" component={LoginScreen} onItemPress={() => {console.log("123")}}/>
    </Drawer.Navigator>
  );
}


