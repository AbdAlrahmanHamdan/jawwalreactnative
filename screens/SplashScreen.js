import React from 'react';
import {
    View,
    AsyncStorage
} from 'react-native';


export default class SplashScreen extends React.Component {

    async componentDidMount() {
        let username = await AsyncStorage.getItem('username');
        let password = await AsyncStorage.getItem('password');
        if (username != null && password != null) {
            this.props.navigation.reset({
                index: 0,
                routes: [{name: 'DrawerScreen'}],
              });
        }
        else {
            this.props.navigation.reset({
                index: 0,
                routes: [{name: 'LoginScreen'}],
            });
        }
    }

    render() {
        return (<View></View>)
    }
}