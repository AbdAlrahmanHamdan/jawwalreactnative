import React from 'react';
import {StyleSheet,View,Text,StatusBar,FlatList} from 'react-native';
import Header from '../components/Header';
import BottomSheet from 'reanimated-bottom-sheet';
import Item from '../components/Item';
import firebase from 'firebase';
import Hand from '../resources/images/waving-hand.svg';

const renderContent = () => (
  <View
    style={{
      backgroundColor: 'white',
      padding: 16,
      height: '100%',
    }}
  >
    <Text>Swipe down to close</Text>
  </View>
);
const sheetRef = React.createRef(null);
export default class MainScreen extends React.Component{
  constructor(props) {
    super(props);
    this.state = {DataArray: []};
  }
    async componentDidMount() {
      var firebaseConfig = {
        apiKey: "AIzaSyDcdng5ReUileInZ-gLu8Y-v4OsORpgmBI",
        authDomain: "jawwal-3513d.firebaseapp.com",
        databaseURL: "https://jawwal-3513d.firebaseio.com",
        projectId: "jawwal-3513d",
        storageBucket: "jawwal-3513d.appspot.com",
        messagingSenderId: "386660872698",
        appId: "1:386660872698:web:c65987d36347d4a1403443",
        measurementId: "G-0XWBXMBYEL"
      };
      // Initialize Firebase
      if (!firebase.apps.length) {
        await firebase.initializeApp(firebaseConfig)
      }
      firebase.database().ref('items').once('value' , async (data) => {
        ItemArray = [];
        for (const [key, value] of Object.entries(data.toJSON())) {
          if (value.checked == true) {
            value.Title = key
            ItemArray.push(value);
          }
        }
        ItemArray.sort(function(a, b){
          return parseInt(a.ID)  - parseInt(b.ID);
         })
        this.setState({DataArray: ItemArray});
      })
  }

  renderHeader = () => (
    <View style={styles.BottomSheetHeaderContainer}>
        <View style={styles.bottomSheetIndicator}></View>
    </View>
  )
  render(){
    return(
      <View style={styles.container}>
        <StatusBar backgroundColor="#6ABF4B"/>
        <Header navigation={this.props.navigation}/>
        <View style={styles.MainScreen}>
          <View style={styles.welcomeMessageContainer}>
            <Hand style={styles.wavingHand} width={50} height={25}/>
            <Text style={styles.welcomeMessage}>اهلا و سهلا بك في E-Dealer</Text>
          </View>
         <FlatList
          keyExtractor={item => item.ID+""}
          data={this.state.DataArray}
          renderItem={({item}) => <Item item={item} navigation={this.props.navigation}/>}
          numColumns={3}/>
        </View>
        <BottomSheet
          ref={sheetRef}
          renderHeader={this.renderHeader}
          snapPoints={[30, 150, '80%']}
          borderRadius={10}
          renderContent={renderContent}
      />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    backgroundColor: '#6ABF4B'
  },
  MainScreen: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25
  },
  welcomeMessageContainer: {
    flexDirection: 'row',
    marginLeft: 65
  },
  welcomeMessage: {
    marginTop: 25,
    fontSize: 24
  },
  wavingHand: {
    marginTop: 30
  },
  BottomSheetHeaderContainer: {
    height: 30,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
bottomSheetIndicator: {
    width: 60,
    height: 6,
    backgroundColor: '#6ABF4B',
    borderRadius: 5
  }
});
