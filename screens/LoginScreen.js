import React from 'react';
import {StyleSheet,View,TextInput,TouchableOpacity,Text,ScrollView, AsyncStorage} from 'react-native';
import BG from '../resources/images/bg.svg'
import GR from '../resources/images/group2.svg'
export default class LoginScreen extends React.Component{

  state = {
    username: '',
    password: ''
  }

  async Login() {
    let username = this.state.username;
    let password = this.state.password;
    if (username != '' && password != '') {
      await AsyncStorage.setItem('username', username);
      await AsyncStorage.setItem('password', password);
      this.props.navigation.navigate('DrawerScreen');
      this.props.navigation.reset({
        index: 0,
        routes: [{name: 'DrawerScreen'}],
      });
    }
  }
  render(){
    return(
      <View style={styles.container}>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
          <BG width={'100%'} height={'100%'} style={styles.backgroundImage}/>
          <View style={styles.icon}>
            <GR/>
          </View>
          <View style={styles.body}>
            <TextInput style={styles.editText} placeholder="أسم المستخدم:" onChangeText={(value) => this.setState({username: value})} value={this.state.username}/>
            <TextInput style={styles.editText} placeholder=":كلمة المرور" secureTextEntry onChangeText={(value) => this.setState({password: value})} value={this.state.password} />
            <TouchableOpacity style={styles.loginButton} onPress= {()=>this.Login()}>
              <Text style={styles.buttonText}>
                تسجيل الدخول
              </Text>
            </TouchableOpacity >
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    top: 0,
    left: 0,
    width: '100%',
    flex: 1,
    flexDirection: 'column'
  },
  backgroundImage: {
    zIndex: -1,
    position: 'absolute',
    flex: 1
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%'
  },
  body: {
    flex: 2,
    width: '100%',
    alignItems: 'center'
  },
  editText: {
    backgroundColor: '#fff',
    width: '70%',
    top: '10%',
    marginBottom: '10%',
    borderColor: '#000',
    borderWidth: 1,
    paddingStart: '15%',
    paddingEnd: '15%',
    textAlign: 'center'
  },
  loginButton: {
    backgroundColor: "#24B4C4",
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    width: '70%',
    height: '8%',
    top: '10%',
    shadowColor: '#000',
    elevation: 5
  },
  buttonText:{
    color: '#ffffff',
    fontSize: 18
  }
});
