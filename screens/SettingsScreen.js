import React from 'react';
import {StyleSheet,View,Text,StatusBar,FlatList,TouchableOpacity} from 'react-native';
import SettingsItem from '../components/SettingsItem';
import firebase from 'firebase';
import Plus from '../resources/images/plus.svg'

export default class MainScreen extends React.Component{
  constructor(props) {
    super(props);
    this.state = {DataArray: [], tempItem:{}};
  }
    async componentDidMount() {
      var firebaseConfig = {
        apiKey: "AIzaSyDcdng5ReUileInZ-gLu8Y-v4OsORpgmBI",
        authDomain: "jawwal-3513d.firebaseapp.com",
        databaseURL: "https://jawwal-3513d.firebaseio.com",
        projectId: "jawwal-3513d",
        storageBucket: "jawwal-3513d.appspot.com",
        messagingSenderId: "386660872698",
        appId: "1:386660872698:web:c65987d36347d4a1403443",
        measurementId: "G-0XWBXMBYEL"
      };
      // Initialize Firebase
      if (!firebase.apps.length) {
        await firebase.initializeApp(firebaseConfig)
      }
      firebase.database().ref('items').once('value' , async (data) => {
        ItemArray = [];
        for (const [key, value] of Object.entries(data.toJSON())) {
            value.Title = key
            ItemArray.push(value);
        }
        ItemArray.sort(function(a, b){
          return parseInt(a.ID)  - parseInt(b.ID);
         })
        this.setState({DataArray: ItemArray});
      })
  }

    saveData() {
        var items = {...this.state.DataArray}
        for (let item in items) {
            let Item = items[item]
            firebase.database().ref('items/'+Item.Title).update({
                checked: Item.checked
            })
        }
        this.props.navigation.reset({
            index: 0,
            routes: [{name: 'DrawerScreen'}],
          });
  }

  render(){
    return(
      <View style={styles.container}>
        <StatusBar backgroundColor="#6ABF4B"/>
        <View style={styles.MainScreen}>
          <View>
            <Text style={styles.welcomeMessage}>قم بأختيار الصفحات التي تريد ظهروها في الصفحة الرئيسية</Text>
          </View>
            <FlatList
            columnWrapperStyle={styles.row}
            keyExtractor={item => item.ID+""}
            data={this.state.DataArray}
            renderItem={({item}) => <SettingsItem item={item} navigation={this.props.navigation}/>}
            numColumns={3}/>
            <TouchableOpacity style={styles.addButton} onPress={() => this.saveData()}>
                <Text style={styles.addButtonText}>أضف هذه الصفحات</Text>
                <Plus width={32}/>
            </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    flexDirection: 'column',
  },
  MainScreen: {
    flex: 1,
    backgroundColor: '#fff',
  },
  welcomeMessage: {
    marginTop: 25,
    fontSize: 16,
    marginEnd: 25
  },
  row: {
    flex: 1,
    justifyContent: "space-around",
    marginTop: 25
  },
  addButton: {
      alignSelf: 'center',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#20A0B9',
      height: 50,
      width: 250,
      borderRadius: 25,
      marginBottom: 15
  },
  addButtonText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 20,
    marginRight: 15
  }
});
