import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity, AsyncStorage} from 'react-native';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import {Title, Caption, Drawer, Text} from 'react-native-paper';
import Homepage from '../resources/images/homepage.svg';
import Settings from '../resources/images/settings.svg';
import Lock from '../resources/images/lock.svg';
import Logout from '../resources/images/logout.svg';
import Arrow from '../resources/images/arrow.svg';
import LineDivider from '../components/LineDivider'
export function DrawerContent(props) {

return(
    <View style={{flex:1}}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View>
            <View style={styles.personalInfo}>
              <View style={styles.personalName}>
                <Title style={styles.title}>Salam Ahmad</Title>
                <Caption style={styles.caption}>Salam Dealer</Caption>
              </View>
              <Image style={styles.profileImage} source={require('../resources/images/profile_picture.png')}/>
            </View>
          </View>

          <LineDivider/>
          <TouchableOpacity style={styles.drawerItem} onPress={() => {props.navigation.closeDrawer()}}>
            <Homepage width={32} height={32} style={styles.drawerItemImage}/>
            <Text style={styles.drawerItemText}>الصفحة الرئيسية</Text>
            <Arrow width={32} height={32} style={styles.drawerItemLeftImage}/>
          </TouchableOpacity>
          <LineDivider/>
          <TouchableOpacity style={styles.drawerItem}>
            <Lock width={32} height={32} style={styles.drawerItemImage}/>
            <Text style={styles.drawerItemText}>تغير كلمة السر</Text>
            <Arrow width={32} height={32} style={styles.drawerItemLeftImage}/>
          </TouchableOpacity>
          <LineDivider/>
          <TouchableOpacity style={styles.drawerItem} onPress={() => {props.navigation.navigate('SettingsScreen')}}>
            <Settings width={32} height={32} style={styles.drawerItemImage}/>
            <Text style={styles.drawerItemText}>الاعدادات</Text>
            <Arrow width={32} height={32} style={styles.drawerItemLeftImage}/>
          </TouchableOpacity>
          <LineDivider/>
          <TouchableOpacity style={styles.drawerItem}
            onPress={() => {
              AsyncStorage.removeItem('username')
              AsyncStorage.removeItem('password')
              props.navigation.navigate('LoginScreen')}}>
            <Logout width={32} height={32} style={styles.drawerItemImage}/>
            <Text style={styles.drawerItemText}>تسجيل الخروج</Text>
            <Arrow width={32} height={32} style={styles.drawerItemLeftImage}/>
          </TouchableOpacity>
          <LineDivider/>
        </View>
      </DrawerContentScrollView>
    </View>
);
}

const styles = StyleSheet.create({
drawerContent: {
  flex: 1,
},
personalInfo: {
  flexDirection:'row',
  margin: 25
},
personalName: {
  flexDirection:'column',
  marginRight: 25
},
profileImage: {
  width: 80,
  height:80
},
title: {
  fontSize: 16,
  marginTop: 3,
  marginRight: 25
},
caption: {
  fontSize: 14,
  lineHeight: 14,
  marginRight: 25
},
row: {
  marginTop: 20,
  flexDirection: 'row',
  alignItems: 'center',
},
section: {
  flexDirection: 'row',
  alignItems: 'center',
  marginRight: 15,
},
paragraph: {
  fontWeight: 'bold',
  marginRight: 3,
},
drawerSection: {
  marginTop: 15,
},
drawerItem: {
  alignItems: 'flex-end',
  flexDirection: 'row-reverse',
  width: '100%',
  height: 50,
  justifyContent: 'space-between'
},
bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1
},
preference: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingVertical: 12,
  paddingHorizontal: 16,
},
drawerItemText: {
  fontSize: 20,
  color: 'black',
  marginRight: 5
},
drawerItemImage: {
  width: 32,
  height: 32,
  marginRight: 20
},
drawerItemLeftImage: {
  marginLeft: 15
}
});