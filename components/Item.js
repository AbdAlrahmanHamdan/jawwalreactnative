import React from 'react';
import {StyleSheet, Text, Image, TouchableOpacity} from 'react-native';

export default class Header extends React.Component{
    constructor(props){
        super(props);
    }
    render() {
        return(
            <TouchableOpacity style={styles.item} onPress={() => {if(this.props.item.ID == 1) {
                this.props.navigation.navigate("SettingsScreen")
            }}}>
                <Image style={styles.image} source={{uri :this.props.item.image}}/>
                <Text numberOfLines={1} style={styles.text}>{this.props.item.name}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    item: {
        margin: 15,
        alignItems: 'center'
    },
    image: {
        height: 100,
        width: 100
    },
    text: {
        fontSize: 16,
        marginTop: 5
    }
});