import React from 'react';
import {StyleSheet, Text, Image, TouchableOpacity} from 'react-native';

export default class Header extends React.Component{

    state = {
        image: '',
        status : 0
      }

      componentDidMount() {
          if (this.props.item.checked == true) {
            this.setState({image: require('../resources/images/checked.png')})
            this.setState({status: 2})
          }
          else {
            this.setState({image: require('../resources/images/not_checked.png')})
            this.setState({status: 1})
          }
      }

      changeCheckedPhoto () {
          if (this.state.status == 1) {
            this.setState({image: require('../resources/images/checked.png')})
            this.setState({status: 2})
            this.props.item.checked = true
          } else {
            this.setState({image: require('../resources/images/not_checked.png')})
            this.setState({status: 1})
            this.props.item.checked = false
          }
      }

    constructor(props){
        super(props);
    }
    render() {
        return(
            <TouchableOpacity style={styles.item} onPress={() => this.changeCheckedPhoto()}>
                <Image style={styles.checkImage} source={this.state.image}/>
                <Image style={styles.image} source={{uri :this.props.item.image}}/>
                <Text numberOfLines={1} style={styles.text}>{this.props.item.name}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    item: {
        alignItems: 'center'
    },
    image: {
        height: 60,
        width: 60
    },
    checkImage: {
        height: 20,
        width: 20,
        marginLeft: 80
    },
    text: {
        fontSize: 12,
        marginTop: 5
    }
});