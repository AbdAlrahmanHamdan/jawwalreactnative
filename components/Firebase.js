import firebase from 'firebase';
export default class Firebase {

    static firebaseConfig = {
        apiKey: "AIzaSyDcdng5ReUileInZ-gLu8Y-v4OsORpgmBI",
        authDomain: "jawwal-3513d.firebaseapp.com",
        databaseURL: "https://jawwal-3513d.firebaseio.com",
        projectId: "jawwal-3513d",
        storageBucket: "jawwal-3513d.appspot.com",
        messagingSenderId: "386660872698",
        appId: "1:386660872698:web:c65987d36347d4a1403443",
        measurementId: "G-0XWBXMBYEL"
      };
      
    static instance = null;

    static createInstance() {
        var object = new Firebase();
        return object;
    }
  
    static getInstance () {
        if (!Firebase.instance) {
            Firebase.instance = Firebase.createInstance();
        }
        return Firebase.instance;
    }

    async getAllData() {
        if (!firebase.apps.length) {
            await firebase.initializeApp(Firebase.firebaseConfig)
          }
          var ItemArray = [];
          firebase.database().ref('items').once('value' , async (data) => {
            for (const [key, value] of Object.entries(data.toJSON())) {
              if (value.checked == true)
                ItemArray.push(value);
            }
            ItemArray.sort(function(a, b){
              return parseInt(a.ID)  - parseInt(b.ID);
             })
          })
          return ItemArray;
    }
}