import React from 'react';
import {View} from 'react-native';
export default class LineDivider extends React.Component{
    render() {
        return(
            <View style={{
                borderBottomColor: '#EAEAEA',
                borderBottomWidth: 1.5,
                marginTop: 15
                }}
            />
        )
    }
}