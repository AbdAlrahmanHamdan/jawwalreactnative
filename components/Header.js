import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Notification from '../resources/images/ic-notification.svg';
import Menu from '../resources/images/ic-menu.svg';
import GP from '../resources/images/group-2.svg';

export default class Header extends React.Component{

    render() {
        return(
            <View style={styles.header}>
                <TouchableOpacity style={styles.menu}  onPress={() => this.props.navigation.toggleDrawer()}>
                    <Menu/>
                </TouchableOpacity>
                <Notification style={styles.notification}/>
                <View onPress={() => this.props.navigation.toggleDrawer()}>
                    <Text style={styles.headerText}>الصفحة الرئيسية</Text>
                </View>
                <GP style={styles.icon}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header:{
        width: '100%',
        height: '10%',
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
        backgroundColor: '#6ABF4B',
        paddingBottom: 15
    },
    headerText:{
        fontWeight: 'bold',
        fontSize: 20,
        color: '#fff',
        letterSpacing: 1
    },
    menu:{
        position: 'relative',
        end: '100%'
    },
    notification:{
        position: 'relative',
        end: '60%'
    },
    icon:{
        position: 'relative',
        start: '100%'
    }
});